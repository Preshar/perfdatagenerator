package com.rotate.data;

/**
 * @author preetesh
 * @project cronjar
 * @createdon 18/10/18
 */



import org.apache.commons.lang3.StringUtils;
import redis.clients.jedis.Jedis;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.ArrayList;
import java.sql.ResultSetMetaData;

class PropLoader
{
    private static Properties properties;

    private static void propertyLoader()
    {
        String filepath="./main.properties";
        properties = new Properties();
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(filepath);
            properties.load(fileInputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static Properties getProperty()
    {
        if(properties==null)
        {
            propertyLoader();
        }
        else
        {
            return properties;
        }
        return properties;
    }
}

class DbFactory {

       public static Connection connection;
       public static Connection makeConnection() {
       Properties properties=PropLoader.getProperty();
       String dburl=properties.getProperty("dburl");
       String dbName=properties.getProperty("dbName");
       String dbUser=properties.getProperty("dbUser");
       String dbPassword=properties.getProperty("dbPassword");

        if (connection==null)
        {
            try {

                String driverName = "com.mysql.jdbc.Driver";
                Class.forName(driverName);

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                return null;
            }
            try {
                //connection = DriverManager.getConnection("jdbc:mysql://delivery-u-cuat-01.swiggyops.de/delivery","root","notarealpassword");
                connection = DriverManager.getConnection(dburl,dbUser,dbPassword);
            } catch (SQLException e) {
                System.out.println("Connection Failed! Check output console");
                e.printStackTrace();
                return null;
            }
            if (connection != null) {
                System.out.println("You made it, take control your database now!");
            } else {
                System.out.println("Failed to make connection!");
            }
            return connection;
        }
        else
        {
            return connection;
        }

    }
    public static List<String> returnQueryDataAsCsvList(String query){
        Statement stm;
        ResultSet rs;
        List <String> batches=new ArrayList<String>();
        Connection conn=makeConnection();

        try {
            if (conn != null) {
                stm = conn.createStatement();
                System.out.println("going to execute the query"+query);
                rs = stm.executeQuery(query);
                String[] arr = null;
                ResultSetMetaData rsmd = rs.getMetaData();
                int numberOfColumns = rsmd.getColumnCount();
                //System.out.println("no of coumns"+numberOfColumns);
                while(rs.next())
                {
                    StringBuilder sb = new StringBuilder();
                    for (int i = 1; i <= numberOfColumns; i++) {
                        try {
                            sb.append(rs.getString(i)+",");
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                    sb.deleteCharAt(sb.length()-1);
                    batches.add(sb.toString());
                }
            }

        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        return batches;

    }}

    public class RedisJmeter {


        String filepath="./main.properties";
        Properties properties=PropLoader.getProperty();
        Integer numBatchRequestToupdate=Integer.parseInt(properties.getProperty("numBatchRequestToupdate"));
        String deList=properties.getProperty("deList");
        String ffOrderList=properties.getProperty("ffOrderList");
        String batchListBulkAssign=properties.getProperty("batchListBulkAssign");
        String orderDeListCaprd=properties.getProperty("orderDeListCaprd");
        Jedis jedis_native= new Jedis(properties.getProperty("redisHost"), 6379);

        public RedisJmeter() {
            properties=PropLoader.getProperty();
            Long orderlen=jedis_native.llen(ffOrderList);
            if(orderlen<numBatchRequestToupdate)
            {
                numBatchRequestToupdate=orderlen.intValue();
            }
/*            properties = new Properties();
            FileInputStream fileInputStream = null;
            try {
                fileInputStream = new FileInputStream(filepath);
                properties.load(fileInputStream);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }*/

        }

        //use connection to get batch_Ids from order_ids
        public  List<String> setKeys() throws SQLException
        {
            System.out.println("Starting--In Method setKeys");
            jedis_native=new Jedis(properties.getProperty("redisHost"), 6379);
            List<String> order_ids= new ArrayList<String>();
            List<String> de_ids= new ArrayList<String>();

            //populate ArrayList by poping orders from orders List
            for(int i=0;i<numBatchRequestToupdate; i++)
            {
                order_ids.add(jedis_native.lpop(ffOrderList));
                de_ids.add(jedis_native.lpop(deList));
            }

            System.out.println("popped orders equal to numBatchRequestToupdate " + numBatchRequestToupdate);
            StringJoiner sj = new StringJoiner(",", "('", "')");
            String onlyOrderskey=properties.getProperty("onlyorders");
            System.out.println("got onlyorders key from properties");
            String ignoredeList=properties.getProperty("ignoredeList");
            System.out.println("got ignoredeList key from properties");
            String batchDeKey=properties.getProperty("batchDeKey");
            System.out.println("got batchDeKey key from properties");

           // System.out.println("pushing orders to only orders key");
            for(String str:order_ids)
            {
                sj.add(str);
            }

            //getting all orders  with their corresponding batch ids, which were popped from orders list
            String joinerstring=sj.toString().replace(",","','");
            String query="select order_id,batch_id from trips where order_id in "+joinerstring;
            List<String> batchList=DbFactory.returnQueryDataAsCsvList(query);
            //List will not be in order of order ids
            List<String> batchreqList=new ArrayList<String>();
            String order_batch_de;
            System.out.println("going to set order batchde key and populating batchrequest List,  size of batchList"+ batchList.size());
            for(int i=0;i<batchList.size(); i++)
            {
                jedis_native.lpush(onlyOrderskey,batchList.get(i).split(",")[0]);
                order_batch_de=batchList.get(i).split(",")[0]+"__"+batchList.get(i).split(",")[1]+"__"+de_ids.get(i);
                System.out.println(order_batch_de+"     Value of order batch de ");
                String batchrequest="{\"batch_id\": "+order_batch_de.split("__")[1]+", \"de_id\": "+order_batch_de.split("__")[2]+", \"source\": \"TestAbhi3\"}";
                batchreqList.add(batchrequest);
                jedis_native.set(order_batch_de.split("__")[0],order_batch_de.split("__")[2]);
                jedis_native.lpush(batchDeKey,batchList.get(i)+","+de_ids.get(i));
            }
            System.out.println("size of batchRequest list came out to be "+batchList.size());

            int i=0;
            int listcounter=0;
            StringBuilder sb = new StringBuilder();
            if(batchList.size()>0) {
                while (true) {
                    if (listcounter == batchreqList.size() - 1) {
                        sb.append(batchreqList.get(listcounter)).append(",");
                        String key = sb.deleteCharAt(sb.length() - 1).toString();
                        jedis_native.lpush(batchListBulkAssign, key);
                        break;
                    }
                    if (i == 30) {
                        i = 0;
                        String key = sb.deleteCharAt(sb.length() - 1).toString();
                        jedis_native.lpush(batchListBulkAssign, key);
                        sb = new StringBuilder();
                    } else {
                        sb.append(batchreqList.get(listcounter)).append(",");
                        listcounter++;
                        i++;
                    }

                    System.out.println("Printing batch  " + sb.toString());
                }
            }
            else
            {
                System.out.println("found no orders so pushing then back to the list");
                for(String order:order_ids)
                {
                    jedis_native.rpush(ffOrderList,order);
                }
            }

        return de_ids;
        }

        /*
        * get all the orders from onlyorders
        * find orders for which assigned time is null
        * find orders for which assigned time is not null
        *
        *  */
        public void addCaprdData()
        {
            jedis_native=new Jedis(properties.getProperty("redisHost"), 6379);
            String key=properties.getProperty("onlyorders");
            int ordersize= Math.toIntExact(jedis_native.llen(key));
            List<String> ordersfromredis=new ArrayList<>();
            for(int i=0;i<ordersize;i++)
            {
                ordersfromredis.add(jedis_native.lpop(key));
            }
            StringJoiner sj = new StringJoiner(",", "('", "')");

            for(String str:ordersfromredis)
            {
                sj.add(str);
            }

            String assignedOrderQuery="select order_id from trips where assigned_time is not null and order_id in "+sj.toString().replace(",","','");
            String unassignedOrderQuery="select order_id from trips where assigned_time is null and order_id in "+sj.toString().replace(",","','");
            List<String> assignedOrders=DbFactory.returnQueryDataAsCsvList(assignedOrderQuery);
            List<String> unAssignedOrders=DbFactory.returnQueryDataAsCsvList(unassignedOrderQuery);
            String caprdkey=properties.getProperty("carprdKey");
            //push assigned in caprd key
            for(String str: assignedOrders)
            {
                jedis_native.lpush(caprdkey,str+","+jedis_native.get(str));
            }

            //push unassigned in orderOnly
            for(String str: unAssignedOrders)
            {
                jedis_native.lpush(key,str);
            }

        }

        public void pushdetoIgnoreList(String de)
        {
            String ignoredeList=properties.getProperty("ignoredeList");
            jedis_native.lpush(ignoredeList,de);
        }

        public void ignorecleanup()
        {
            String ignoredeList=properties.getProperty("ignoredeList");
            String orderprefix=properties.getProperty("orderprefix");
            Long len=jedis_native.llen(ignoredeList);
            if(len>0)
            {
                List<String> deIdList=jedis_native.lrange(ignoredeList,0,len);
                StringJoiner sj = new StringJoiner(",", "('", "')");
                for(String str:deIdList)
                {
                    sj.add(str);
                }
                String joinerstring=sj.toString().replace(",","','");
                List<String> freedes=DbFactory.returnQueryDataAsCsvList("select de_id from trips where delivered_time >= DATE_SUB(NOW(),INTERVAL 1 HOUR) and delivered_time is not null and de_id in "+joinerstring);
                if(freedes.size()>0)
                {
                    List<String> union = new ArrayList<String>(deIdList);
                    union.addAll(freedes);
                    List<String> intersection = new ArrayList<String>(deIdList);
                    intersection.retainAll(freedes);
                    union.removeAll(intersection);
                    jedis_native.del(ignoredeList);
                    for(String str:union)
                    {
                        jedis_native.lpush(ignoredeList,str);
                    }

                }

            }
        }
        //update batch in redis
        //pop all batch in redis, check if

    public static void main(String[] args) {
       RedisJmeter redisJmeter= new RedisJmeter();


        //redisJmeter.datafeed();
        List<String> deIgnoreList=null;

            try {
                redisJmeter.deleteDesAndUpdate();
                }
            catch(Exception e){
                e.printStackTrace();
            }

            try {
                 deIgnoreList = redisJmeter.setKeys();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            try {
                redisJmeter.addCaprdData();
            }catch (Exception e)
            {
                e.printStackTrace();
            }

            try
            {
                redisJmeter.ignorecleanup();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            try{
            for(String des:deIgnoreList)
            {
                redisJmeter.pushdetoIgnoreList(des);
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        try {
            DbFactory.connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteDesAndUpdate()
    {
        String ignoredeList=properties.getProperty("ignoredeList");
        jedis_native=new Jedis(properties.getProperty("redisHost"),6379);
        Long len=jedis_native.llen(ignoredeList);

        List<String> deList=null;
        String joinerstring=null;

        if(len>0)
        {
            List<String> deIdList=jedis_native.lrange(ignoredeList,0,len);
            StringJoiner sj = new StringJoiner(",", "('", "')");
            for(String str:deIdList)
                {
                    sj.add(str);
                }
        joinerstring=sj.toString().replace(",","','");
        deList = DbFactory.returnQueryDataAsCsvList("select id from delivery_boys where enabled =1 and status='FR' and batch_id is null and id not in" +joinerstring+ "limit " +numBatchRequestToupdate);
        }
        else
        {
            deList = DbFactory.returnQueryDataAsCsvList("select id from delivery_boys where enabled =1 and status='FR' and batch_id is null limit " +numBatchRequestToupdate);
        }

        jedis_native.del(properties.getProperty("deList"));
        for(String de:deList)
        {
            jedis_native.lpush(properties.getProperty("deList"),de);
        }

    }

    public void addDeModile()
    {

        String mobileList=properties.getProperty("mobilekey");
        jedis_native=new Jedis(properties.getProperty("redisHost"),6379);
        Long len=jedis_native.llen(mobileList);
        List<String> mobiles= new ArrayList<>();
        if(len==0)
            mobiles=DbFactory.returnQueryDataAsCsvList("select mobile from delivery_boys where enabled=1 and mobile is not null order by id desc limit 10000");
        for(String mob:mobiles)
        {
            jedis_native.lpush(mob);
        }
    }
    public  void datafeed()
    {
        String query="select order_id from trips where assigned_time is not null order by 1 desc limit "+numBatchRequestToupdate;
        List<String>order_ids=DbFactory.returnQueryDataAsCsvList(query);
        jedis_native=new Jedis("localhost", 6379);
        for(String order:order_ids) {
            System.out.println(order);
        }
        for(String order:order_ids) {
            jedis_native.lpush(ffOrderList,order);
            jedis_native.lpush(deList,order);
        }

    }

}

